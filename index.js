import { Navigation } from 'react-native-navigation';
import { registerScreens } from './src/screens';
import Icon from 'react-native-vector-icons/Ionicons';

registerScreens(); // this is where you register all of your app's screens
Promise.all([
    Icon.getImageSource("ios-home", 30,),
    Icon.getImageSource("ios-list", 30),
    Icon.getImageSource("ios-add", 30),
  ]).then(sourses => {
    // start the app
    Navigation.startTabBasedApp({
    tabs: [
        {
        label: 'Projects',
        screen: 'app.ProjectsScreen', 
          icon: sourses[0],
        title: 'Projects',
        navigatorButtons: {
            rightButtons: [
                {
                    title: 'add new',
                    icon: sourses[2],
                    id: 'addProject'
                }
            ]
        }
        },
        {
        label: 'Invoice',
        screen: 'app.InvoiceScreen',
        icon: sourses[1],
        title: 'Invoice'
        }
    ]
    })
});