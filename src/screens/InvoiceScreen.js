import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';
import { db } from './../config/db';
import { timeToTotal } from './../config/helper';

class InvoiceScreen extends Component {
    constructor(props) {
        super(props);
        this.ref = db.ref('projects/');
        this.state = {
            projects: []
        }
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    }
    onNavigatorEvent = event => {
        if(event.type === "NavBarButtonPress"){
          if(event.id === "addProject") {
            this.props.navigator.showModal({
                screen: "app.AddProject", 
                title: "Add New Project", 
                passProps: {}, 
                navigatorStyle: {},
                animationType: 'slide-up'
              });
          }
        }
        if (event.id === 'willAppear') {  
            this.ref.once("value")
            .then((snapshot) =>  {
                snapshot.forEach((childSnapshot) =>  {
                    let key = childSnapshot.key;
                    let childData = childSnapshot.val();
                    let totalTimeString = '';
                    if (childData) {
                        let logsTime = [];
                        for(let index in childData.logs){
                            logsTime.push(childData.logs[index].time);
                        }
                        totalTimeString = timeToTotal(logsTime);
                    }
                    let newChild = { id: key, name: childData.projectName, totalTime: totalTimeString }
                    
                    this.setState({
                        projects: [...this.state.projects, newChild]
                    })
                });
                
            });
        }
        if (event.id === 'willDisappear') { 
            this.setState({
                projects: []
            })
        }
    }
    _renderItem = ({item}) => {
        return (
            <View style={styles.singleProjectWrapper}>
                <View style={styles.nameWrapper}>
                    <Text style={styles.name}>{item.name}</Text>
                </View>
                <View style={styles.hoursWrapper}>
                        <Text style={styles.hoursText}>{item.totalTime} Hours</Text>                    
                </View>
            </View>
        )
    }
    render = () => {
        return (
            <View style={styles.wrapper}>
               <FlatList
                    data={this.state.projects}
                    extraData={this.state.projects}
                    keyExtractor={item => item.id.toString()}
                    renderItem={this._renderItem}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1
    },
    singleProjectWrapper: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        padding: 10, 
        borderBottomWidth: 3, 
        borderBottomColor: '#F9F9F9'
    },
    nameWrapper: {
        flexDirection: 'row', 
        justifyContent: 'flex-start', 
        padding: 5
    },
    name: {
        fontWeight: 'bold'
    },
    hoursWrapper: {
        padding: 5
    },
    hoursText: {
        fontWeight: 'bold', 
        color: '#A9A9A9'
    }
})
export default InvoiceScreen;