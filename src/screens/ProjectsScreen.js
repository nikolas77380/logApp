import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { db } from './../config/db';
import { totalStringTime } from './../config/helper';

class ProjectsScreen extends Component {
    constructor(props){
        super(props)
        this.ref = db.ref('projects/');
        this.state = {
            projects: []
        }
     this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }
  onNavigatorEvent = event => {
    if(event.type === "NavBarButtonPress"){
      if(event.id === "addProject") {
        this.props.navigator.showModal({
            screen: "app.AddProject", 
            title: "Add New Project", 
            passProps: {}, 
            navigatorStyle: {},
            animationType: 'slide-up'
          });
      }
    }
    if (event.id === 'willAppear') {  
        this.ref.once("value")
        .then((snapshot) =>  {
            snapshot.forEach((childSnapshot) =>  {
                let key = childSnapshot.key;
                let childData = childSnapshot.val();
                let totalTimeString = '';
                if (childData.logs) {
                    let logsTime = [];
                    for(let index in childData.logs){
                        logsTime.push(childData.logs[index].time);
                    }
                    totalTimeString = totalStringTime(logsTime);
                }
                let newChild = { id: key, name: childData.projectName, totalTime: totalTimeString }
                this.setState({
                    projects: [...this.state.projects, newChild]
                })
            });
            
        });
    }
    if (event.id === 'willDisappear') { 
        this.setState({
            projects: []
        })
    }
  }

  timeToTotal = (time) => {
    
  }

  openProject = (projectId) => {
        let selectedProject = this.state.projects.filter((item) => item.id === projectId)[0];
        this.setState({
            projects: []
        })
        this.props.navigator.push({
            title: selectedProject.name,
            backButtonTitle: '',
            screen: 'app.SingleProject',
            animated: true,
            animationType: 'slide',
            passProps: {
                selectedProject
            }
          });
    }
    _renderItem = ({item}) => {
        return (
            <TouchableOpacity onPress={() => this.openProject(item.id)}>
                <View style={styles.singleProjectWrapper}>
                    <View style={styles.nameWrapper}>
                        <Text style={styles.name}>{item.name}</Text>
                    </View>
                    <View style={styles.rightPart}>
                        <View style={styles.hoursWrapper}>
                            <Text style={styles.hoursText}>{item.totalTime}</Text>
                        </View>
                        <View>
                            <Icon size={25} name="ios-arrow-forward" color='#A9A9A9'/>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
    render = () => {
        return (
            <View style={{flex: 1}}>
               <FlatList
                    data={this.state.projects}
                    extraData={this.state.projects}
                    keyExtractor={item => item.id.toString()}
                    renderItem={this._renderItem}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    wrapper: {
        flex: 1
    },
    singleProjectWrapper: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        padding: 10, 
        borderBottomWidth: 3, 
        borderBottomColor: '#F9F9F9'
    },
    nameWrapper: {
        flexDirection: 'row', 
        justifyContent: 'flex-start', 
        padding: 5
    },
    name: {
        fontWeight: 'bold'
    },
    hoursWrapper: {
        marginRight: 5,
        padding: 5
    },
    hoursText: {
        fontWeight: 'bold', 
        color: '#A9A9A9'
    },
    rightPart: {
        flexDirection: 'row', 
        justifyContent: 'space-between'
    }
})

export default ProjectsScreen;