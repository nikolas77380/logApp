import { Navigation } from 'react-native-navigation';
import SingleProject from './SingleProject';
import ProjectsScreen from './ProjectsScreen';
import InvoiceScreen from './InvoiceScreen';
import AddProject from './AddProject';

// register all screens of the app (including internal ones)
export function registerScreens() {
  Navigation.registerComponent('app.ProjectsScreen', () => ProjectsScreen);
  Navigation.registerComponent('app.AddProject', () => AddProject);
  Navigation.registerComponent('app.SingleProject', () => SingleProject);
  Navigation.registerComponent('app.InvoiceScreen', () => InvoiceScreen);
}