import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, FlatList, Alert, StyleSheet } from 'react-native';
import { db } from './../config/db';
import { today } from './../config/helper';

class SingleProject extends Component {
    constructor(props) {
        super(props);
        this.state = {
            description: '',
            time: '',
            logs: []
        }
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    }
    onNavigatorEvent = event => {
        if (event.id === 'willAppear') {  
            db.ref(`projects/${this.props.selectedProject.id}/logs`).on('value', (snapshot) => {
                snapshot.forEach(childSnapshot => {
                    let key = childSnapshot.key;
                    let childData = childSnapshot.val();
                    let newLog = { id: key, description: childData.description, time: childData.time, createdAt: childData.createdAt }
                    this.setState({
                        logs: [...this.state.logs, newLog]
                    })
                });
            })
        }
        if ( event.id === 'willDisappear') {
            this.setState({
                logs: []
            })
            // this.ref = undefined;
        }
      }
    timeInputHandler = (time) => {
        this.setState({ time });
    }
    descriptionInputHandler = (description) => {
        this.setState({ description });
    }
    submitLog = () => {
        let newLog = { description: this.state.description, time: this.state.time, createdAt: today() };

        if(!this.state.time) {
            Alert.alert('', 'Time is Reqired',[
                {text: 'OK'}  
              ]);
            return false;
        } else {
            db.ref(`projects/${this.props.selectedProject.id}/logs`).push(newLog);
            this.setState({
                description: '',
                time: ''
            })
        }
    }
    _renderSingleLog = ({item}) => {
        return (
            <View style={styles.singleLogWrapper}>
                <View style={styles.singleLogTimeWrapper}>
                    <View style={styles.flexDirection}>
                        <Text style={styles.naText}>Time spent: </Text><Text>{item.time}</Text>
                    </View>
                    <View>
                        <Text style={styles.createdAtText}>{item.createdAt}</Text>
                    </View>
                </View>
                <View>
                    <Text>{item.description}</Text>
                </View>
            </View>
        )
    }
    renderActivity = () => {
        if(this.state.logs.length) {
            return (
                <View style={{flex:1}}>
                    <FlatList
                        data={this.state.logs}
                        extraData={this.state.logs}
                        keyExtractor={item => item.id.toString()}
                        renderItem={this._renderSingleLog}
                    />
                </View>
            )
        }
        return (
            <Text style={styles.naText}>No Activities</Text>
        )
    }
    render() {
        return (
            <View style={styles.Container}>
                <View style={styles.inputsWrapper}>
                    <View style={styles.timeInputWrapper}>
                        <TextInput 
                            style={styles.timeInput} 
                            value={this.state.time} 
                            onChangeText={(value) => this.timeInputHandler(value)} />
                        <Text style={{fontSize: 18}}>(e.g 2w3d4h)</Text>
                    </View>
                    <View style={styles.descriptionWrapper}>
                        <Text style={styles.title}>Description</Text>
                        <TextInput  
                            style={styles.descriptionInput}
                            value={this.state.description} 
                            onChangeText={(value) => this.descriptionInputHandler(value)} 
                            multiline = {true}
                            numberOfLines = {4}
                        />
                    </View>
                </View>
                <View style={styles.buttonWrapper}>
                    <TouchableOpacity onPress={() => this.submitLog()}>
                        <View style={styles.logButton}>
                            <Text style={styles.logText}>Log time</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.activitiesWrapper}>
                    <Text style={styles.title}>Activity</Text>
                </View>
                {this.renderActivity()}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    Container: {
        flex: 1,
        padding: 5
    },
    inputsWrapper: {
        justifyContent: 'flex-start', 
        margin: 5
    },
    timeInputWrapper: {
        flexDirection: 'row'
    },
    timeInput: {
        width: 120, 
        height: 25, 
        borderWidth: 1, 
        borderColor: '#A9A9A9'
    },
    descriptionWrapper: {
        width: '100%',
        marginTop: 10
    },
    title: {
        fontSize: 18, 
        fontWeight: 'bold'
    },
    descriptionInput: {
        borderWidth: 1, 
        height: 100, 
        borderColor: '#A9A9A9'
    },
    buttonWrapper: {
        margin: 5, 
        alignItems: 'flex-start', 
        justifyContent: 'flex-start'
    },
    logButton: {
        padding: 10,
        backgroundColor: '#5cb85c'
    },
    logText: {
        color: '#fff'
    },
    activitiesWrapper: {
        margin: 5, 
        padding: 5, 
        alignItems: 'center', 
        justifyContent: 'flex-start', 
        borderTopWidth: 1, 
        borderColor: '#A9A9A9'
    },
    naText: {
        color: '#A9A9A9'
    },
    singleLogWrapper: {
        borderBottomWidth: 1, 
        borderColor: '#A9A9A9', 
        padding: 5
    },
    singleLogTimeWrapper: {
        flexDirection: 'row', 
        marginBottom: 5, 
        justifyContent: 'space-between'
    },
    flexDirection: {
        flexDirection: 'row'
    },
    createdAtText: { 
        fontSize: 12
    }
})
export default SingleProject;