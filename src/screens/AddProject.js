import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, Alert, StyleSheet } from 'react-native';
import {db} from './../config/db';

class AddProject extends Component {
    constructor(props) {
        super(props);
        this.state = {
            projectName: '', 
        }
    }
    projectNameHandler = (value) => {
        this.setState({
            projectName: value
        })
    }

    submitAddProject = () => {
        let { projectName } = this.state; 
        if(projectName.length) {
            db.ref('projects/').push({
                projectName,
                logs: []
            });
            this.setState({
                projectName: ''
            });
            this.decline()
        } else {
            Alert.alert('', 'Project name is Reqired',[
                {text: 'OK', onPress: () => {
                  
                }}  
              ]);
            return false;
        }
    }

    decline = () => {
        this.props.navigator.dismissModal({
            animationType: 'slide-down'
          });
    }

    render = () => {
        return (
            <View style={styles.Container}>
                <View>
                    <View style={styles.titleWrapper}>
                        <Text style={styles.title}>Project Name</Text>
                    </View>
                    <TextInput 
                        style={styles.input} 
                        value={this.state.projectName} 
                        onChangeText={(value) => this.projectNameHandler(value)} />
                </View>
                <View style={styles.buttonsWrapper}>
                    <TouchableOpacity style={styles.margin} onPress={() => this.submitAddProject()}>
                        <View style={styles.SuccessButtonStyle}>
                            <Text style={styles.buttonText}>Add</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.margin} onPress={() => this.decline()}>
                        <View style={styles.CancelButtonStyle}>
                            <Text style={styles.buttonText}>Cancel</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    Container: {
        flex: 1,
        justifyContent: 'center', 
        alignItems: 'center'
    },
    titleWrapper: {
        marginBottom: 10
    },
    title: {
        fontSize: 18, 
        fontWeight: 'bold'
    },
    input: {
        width: 120, 
        height: 25, 
        borderWidth: 1, 
        borderColor: '#A9A9A9'
    }, 
    buttonsWrapper: {
        marginTop: 5, 
        flexDirection: 'row', 
        justifyContent: 'space-around',
    },
    margin: {
        margin: 5,
    },
    SuccessButtonStyle: {
        padding: 10, 
        backgroundColor: '#5cb85c'
    },
    CancelButtonStyle: {
        padding: 10, 
        backgroundColor: '#d9534f'
    },
    buttonText: {
        color: '#fff'
    }

})
export default AddProject;