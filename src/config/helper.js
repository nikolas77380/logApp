export const timeToTotal = (time) => {

    let text = time.join('');
    let totalHours = 0;
    let result = text.match(/([0-9]+[wdh][]*)/g);
    result.forEach(item => {
        if( item.indexOf('w') != -1 ) {
            totalHours += +item.replace('w', '')*40
        } else if (item.indexOf('d') != -1) {
            totalHours += +item.replace('d', '')*8;
        } else if (item.indexOf('h') != -1) {
            totalHours += +item.replace('h', '');
        }
    });
    return totalHours;
}

export const totalStringTime = (time) => {

    let totalTime = timeToTotal(time);
    let weeks = Math.floor(totalTime/40);
    var days=Math.floor((totalTime - (weeks*40))/8);
    let hours = totalTime - weeks*40 - days*8;
    let result = '';
    if( weeks !== 0 ) {
        result = weeks+ ' weeks ';
    }
    if( days !== 0 ) {
        result = result+days+' days ';
    }
    if( hours !== 0 ) {
        result = result+hours+ ' hours';
    }
    return result;
}

export const today = () => {
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth()+1; //January is 0!
    let yyyy = today.getFullYear();

    if(dd<10) {
        dd = '0'+dd
    } 

    if(mm<10) {
        mm = '0'+mm
    } 

    today = mm + '/' + dd + '/' + yyyy;
    return today;
}